﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad2._7
{
    class Program
    {
        static bool rok (int a)
        {
            if(((a % 4 == 0) && (a % 100 != 0)) || (a % 400 == 0))
            {
                Console.WriteLine("Rok jest przestepny");
                return true;
            }
            else
            {
                Console.WriteLine("Rok nie jest przestepny");
                return false;
            }
        }
        static void Main(string[] args)
        {
            int a;
            Console.WriteLine("Podaj rok, program sprawdzi czy jest przestepny");
            a = Convert.ToInt32(Console.ReadLine());
            rok(a);
            Console.ReadKey();
        }
    }
}
