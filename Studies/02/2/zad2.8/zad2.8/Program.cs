﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad2._8
{
    class Program
    {
        static bool pitagorejski(int a, int b, int c)
        {
            if (a * a + b * b == c * c)
            {
                Console.WriteLine("Trojkat jest pitagorejski");
                return true;
            }
            else
            {
                Console.WriteLine("Trojkat nie jest pitagorejski");
                return false;
            }
        }
        static void Main(string[] args)
        {
            int a, b, c;
            Console.WriteLine("Program sprawdza czy trojkat jest pitagorejski - podaj dlugosci bokow");
            a = Convert.ToInt32(Console.ReadLine());
            b = Convert.ToInt32(Console.ReadLine());
            c = Convert.ToInt32(Console.ReadLine());
            pitagorejski(a, b, c);
            Console.ReadKey();
        }
    }
}
