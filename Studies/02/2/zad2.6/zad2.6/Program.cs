﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad2._6
{
    class Program
    {
        static void tabliczka (int a)
        {
            for(int i=1; i<=a; i++)
            {
                for (int j = 1; j <= a ; j++)
                {
                    Console.Write("{0,5}",i*j);
                }
                Console.WriteLine();
            }
        }
        static void Main(string[] args)
        {
            int a;
            Console.WriteLine("Podaj liczbe, program wyswietli tabliczke mnozenia");
            a = Convert.ToInt32(Console.ReadLine());
            
            tabliczka(a);
            Console.ReadKey();
        }
    }
}
