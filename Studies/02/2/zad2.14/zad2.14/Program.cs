﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad2._14
{
    class Program
    {
        static bool binary(int a)
        {
            int b, i=0;
            var list = new List<int>();
            while (a > 0)
            {
                
                
                b = a % 2;
                list.Add(b);
                a = a / 2;
                
            }
            list.Reverse();
            list.ForEach(Console.Write);
            return true;
        }
        static void Main(string[] args)
        {
            int a;
            Console.WriteLine("Podaj liczbe, program zamieni ja na postac binarna");
            a = Convert.ToInt32(Console.ReadLine());
            binary(a);
            Console.ReadKey();
        }
    }
}
