﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad2._12
{
    class Program
    {
        static int power(int a)
        {
            int wynik = 1;
            for (int i = 0; i < a; i++)
            {
                wynik *= 2;
            }
            return wynik;
        }
        static void Main(string[] args)
        {
            int a;
            Console.WriteLine("Program obliczy nta potege liczby 2");
            a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(power(a));
            Console.ReadKey();
        }
    }
}
