﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad2._4
{
    class Program
    {
        static bool pierwsza(int a)
        {
            if (a < 2)
            {
                Console.WriteLine("Liczba nie jest liczba pierwsza");
                return false;
            }
            else
            {
                for(int i = 2; i*i<=a; i++)
                {
                    if (a % i == 0)
                    {
                        Console.WriteLine("Liczba nie jest pierwsza");
                        return false;
                    }
                }
                Console.WriteLine("Liczba jest liczba pierwsza");
                return true;
            }
        }
        static bool dodatnia(int a)
        {
            if (a > 0)
            {
                Console.WriteLine("Liczba jest dodatnia");
                return true;
            }
            else
            {
                Console.WriteLine("Liczba nie jest dodatnia");
                return false;
            }
        }
        static bool zlota(int a)
        {
            int suma=0;
            for(int i=1; i<a; i++)
            {
                if (a % i == 0)
                {
                    suma = suma + i;
                }
            }
            if (suma == a)
            {
                Console.WriteLine("Liczba jest liczba zlota");
                return true;
            }
            else
            {
                Console.WriteLine("Liczba nie jest liczba zlota");
                return false;
            }

        }
        static void Main(string[] args)
        {
            int a;
            Console.WriteLine("Podaj liczbe, program sprawdzi czy jest pierwsza, dodatnia i zlota");
            a = Convert.ToInt32(Console.ReadLine());
            pierwsza(a);
            dodatnia(a);
            zlota(a);
            Console.ReadKey();
        }
    }
}
