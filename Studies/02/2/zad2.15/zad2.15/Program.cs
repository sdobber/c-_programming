﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad2._15
{
    class Program
    {
        static bool binary (int a)
        {
            int b = 0;
            double licznik = 0.0, wynik = 0.0;
            while (a > 0)
            {
                b = a % 2;
                a = a / 10;
                wynik += (b * Math.Pow(2.0, licznik));
                licznik++;
            }
            Console.WriteLine(wynik);
            return true;
        }
        static void Main(string[] args)
        {
            int a;
            Console.WriteLine("Podaj liczbe binarnie. Program zamieni ja na dziesietna");
            a = Convert.ToInt32(Console.ReadLine());
            binary(a);
            Console.ReadKey();
        }
    }
}
