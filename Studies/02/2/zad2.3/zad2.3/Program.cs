﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad2._3
{
    class Program
    {
        static int kwadrat (int a)
        {
            return a * a;
        }
        static int prostokat(int a, int b)
        {
            return a * b;
        }
        static double trojkatprst(int a, int h)
        {
            return 0.5 * a * h;
        }
        static double kolo(int r)
        {
            return Math.PI * r * r;
        }
        static void Main(string[] args)
        {
            int a, b, h, r;
            Console.WriteLine("Podaj 4 liczby, bok, bok, wysokosc i promien, program obliczy pola");
            a = Convert.ToInt32(Console.ReadLine());
            b = Convert.ToInt32(Console.ReadLine());
            h = Convert.ToInt32(Console.ReadLine());
            r = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Pole kwadratu jest rowne {0}", kwadrat(a));
            Console.WriteLine("Pole prostokatu jest rowne {0}", prostokat(a,b));
            Console.WriteLine("Pole trojkata prostokatnego jest rowne {0}", trojkatprst(a,h));
            Console.WriteLine("Pole kola jest rowne {0}", kolo(r));
            Console.ReadKey();
        }
    }
}
