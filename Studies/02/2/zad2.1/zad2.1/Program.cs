﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad2._1
{
    class Program
    {
        static int suma(int a, int b)
        {
            return a + b;
        }
        static int roznica(int a, int b)
        {
            return a - b;
        }
        static int iloczyn(int a, int b)
        {
            return a * b;
        }
        static int iloraz(int a, int b)
        {
            return a / b;
        }
        static void Main(string[] args)
        {
            int a, b;
            Console.WriteLine("Podaj dwie liczby, program wykona na nich dzialania");
            a = Convert.ToInt32(Console.ReadLine());
            b = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Suma liczb {0} i {1} jest rowna {2}", a, b, suma(a,b));
            Console.WriteLine("Roznica liczb {0} i {1} jest rowna {2}", a, b, roznica(a, b));
            Console.WriteLine("Iloczyn liczb {0} i {1} jest rowna {2}", a, b, iloczyn(a, b));
            Console.WriteLine("Iloraz liczb {0} i {1} jest rowna {2}", a, b, iloraz(a, b));
            Console.ReadKey();
        }
    }
}
