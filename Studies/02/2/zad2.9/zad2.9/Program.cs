﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad2._9
{
    class Program
    {
        static int silnia(int a)
        {
            if (a==0 || a == 1)
            {
                return 1;
            }
            else
            {
                return a * silnia(a - 1);
            }
        } 
        static void Main(string[] args)
        {
            int a;
            Console.WriteLine("Podaj liczbe");
            a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(silnia(a));
            Console.ReadKey();
        }
    }
}
