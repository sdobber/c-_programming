﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad2._5
{
    class Program
    {
        static double liniowa(int a, int b)
        {
            return ((-b) * 1.0) / a;
        }
        static void kwadratowa(int a, int b, int c)
        {
            double mzerowe;
            double mzerowe1;
            double mzerowe2;
            int delta = b * b - 4 * a * c;
            if (delta < 0)
            {
                Console.WriteLine("Brak miejsca zerowego");
            }
            else if (delta == 0)
            {
                mzerowe = (-b) * 1.0 / (2 * a);
                Console.WriteLine(mzerowe);
            }
            else
            {
                mzerowe1 = ((-b) * 1.0 - Math.Sqrt(delta)) / (2 * a);
                mzerowe2 = ((-b) * 1.0 + Math.Sqrt(delta)) / (2 * a);
                Console.WriteLine("{0} {1}", mzerowe1, mzerowe2);
            }
        }
        static void Main(string[] args)
        {
            int a, b, c;
            Console.WriteLine("Podaj wspolczynniki funkcji - a, b, c. Program obliczy miejsca zerowe");
            a = Convert.ToInt32(Console.ReadLine());
            b = Convert.ToInt32(Console.ReadLine());
            c = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(liniowa(a, b));
            kwadratowa(a, b, c);
            Console.ReadKey();
        }
    }
}
