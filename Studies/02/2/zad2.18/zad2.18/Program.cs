﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad2._18
{
    class Program
    {
        static bool checkword(string a)
        {
            int flag = 0;
            for (int i = 0; i < ((a.Length) / 2); i++)
            {
                if (a[i] == a[a.Length - (i + 1)])
                {
                    flag++;
                }
                else
                {
                    flag = 0;
                }
            }
            if (flag == (a.Length / 2))
            {
                Console.WriteLine("Slowo jest palindromem");
            }
            else
            {
                Console.WriteLine("Slowo nie jest palindromem");
            }
            return true;
        }
        static void Main(string[] args)
        {
            string a;
            Console.WriteLine("Podaj slowo. Program sprawdzi czy jest palindromem.");
            a = Console.ReadLine();
            checkword(a);
            Console.ReadKey();
        }
    }
}
