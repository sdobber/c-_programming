﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad2._11
{
    class Program
    {
        static int wiek(int a)
        {
            int b = (int)DateTime.Now.Year;
            return b-a;
        }
        static void Main(string[] args)
        {
            int a;
            Console.WriteLine("Podaj rok urodzenia; program obliczy ile masz lat");
            a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(wiek(a));
            Console.ReadKey();
        }
    }
}
