﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad3._1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Logarytm o podstawie 10 z 1000 to: {0}", Counting.Logarytm());
            Console.WriteLine("Liczba Pi: {0}", Counting.LiczbaPi());
            Console.WriteLine("Wartość bezwzględna z liczby -55 to: {0}", Counting.WartoscBezwzgledna());
            Console.WriteLine("Cosinus 45 stopni to: {0}", Counting.Cosinus());
            Console.WriteLine("Liczba 2 podniesiona do potęgi 3 to: {0}", Counting.Potega());
            Console.WriteLine("Pierwiastek kwadratowy z liczby 16 to: {0}", Counting.Pierwiastek());
            Console.WriteLine("Liczba maksymalna z liczb 3 i 4 to: {0}", Counting.Maksymalna());
            Console.ReadKey();
        }
    }
    class Counting
    {
        public static double Logarytm()
        {
            return Math.Log10(1000);
        }
        public static double LiczbaPi()
        {
            return Math.PI;
        }
        public static int WartoscBezwzgledna()
        {
            return Math.Abs(-55);
        }
        public static double Cosinus()
        {
            return Math.Cos(45);
        }
        public static double Potega()
        {
            return Math.Pow(2.0, 3.0);
        }
        public static double Pierwiastek()
        {
            return Math.Sqrt(16);
        }
        public static int Maksymalna()
        {
            return Math.Max(3, 4);
        }
    }
}
