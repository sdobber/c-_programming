﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad3._8
{
    class Program
    {
        static void Main(string[] args)
        {
            Meble mebel1 = new Meble("brazowy", "dab", "zloty", 100);
            Meble mebel2 = new Meble("czarny", "klon", "zloty", 120);
            Meble mebel3 = new Meble("bialy", "brzoza", "zloty", 130);
            mebel1.DisplayValues();
            mebel2.DisplayValues();
            mebel3.DisplayValues();
            Console.ReadKey();
        }
        struct Meble
        {
            private string kolor, tworzywo, waluta;
            private int cena;
            public Meble(string kolor_, string tworzywo_, string waluta_, int cena_)
            {
                kolor = kolor_;
                tworzywo = tworzywo_;
                waluta = waluta_;
                cena = cena_;
            }
            public void DisplayValues()
            {
                Console.WriteLine("{0} {1} {2} {3}", kolor, tworzywo, waluta, cena);
            }
        }
    }
}
