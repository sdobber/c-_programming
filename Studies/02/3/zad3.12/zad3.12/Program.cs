﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad3._12
{
    class Program
    {
        static void Main(string[] args)
        {
            Kwadrat kwadrat1 = new Kwadrat(7.82);
            kwadrat1.Display();
            Console.ReadKey();
        }
    }
    class Kwadrat
    {
        public double a;
        public Kwadrat(double a_)
        {
            a = a_;
        }
        public void Display()
        {
            Console.WriteLine("{0} {1}", a, a*a);
        }
    }
}
