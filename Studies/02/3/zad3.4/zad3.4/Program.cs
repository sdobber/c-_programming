﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad3._4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(metoda());
            Console.WriteLine(metoda(10));
            Console.WriteLine(metoda("test"));
            Console.WriteLine(metoda(2.5, 5.7));
            Console.ReadKey();
        }
        public static string metoda()
        {
            return "Metoda bez argumentów";
        }

        public static string metoda(int x)
        {
            return "Metoda z jednym argumentem typu int";
        }

        public static string metoda(string x)
        {
            return "Metoda z jednym argumentem typu string";
        }

        public static string metoda(double x, double y)
        {
            return "Metoda z dwoma argumentami typu double";
        }
    }
}
