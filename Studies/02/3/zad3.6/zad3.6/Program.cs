﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad3._6
{
    class Program
    {
        static void Main(string[] args)
        {
            Ksiazka ksiazka1 = new Ksiazka("J. R. R. Tolkien", "Dzieci Hurina", "PWN", 1, 1996);
            Ksiazka ksiazka2 = new Ksiazka("Terry Pratchett", "Mort", "PWN", 2, 1986);
            Ksiazka ksiazka3 = new Ksiazka("Terry Pratchett", "Zlodziej czasu", "PWN", 3, 1987);
            ksiazka1.Display();
            ksiazka2.Display();
            ksiazka3.Display();
            Console.ReadKey();
        }
    }

    class Ksiazka
    {
        private string autor, tytul, wydawnictwo;
        private int id_ksiazki, rok_wydania;

        public Ksiazka(string autor_, string tytul_, string wydawnictwo_, int id_ksiazki_, int rok_wydania_)
        {
            autor = autor_;
            tytul = tytul_;
            wydawnictwo = wydawnictwo_;
            id_ksiazki = id_ksiazki_;
            rok_wydania = rok_wydania_;

        }
        
        public void Display()
        {
            Console.WriteLine("{0} {1} {2} {3} {4}", autor, tytul, wydawnictwo, id_ksiazki, rok_wydania);
        }
    }
}
