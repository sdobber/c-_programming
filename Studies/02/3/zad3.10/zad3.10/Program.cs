﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad3._10
{
    class Program
    {
        static void Main(string[] args)
        {
            Fibonacci fibonacci1 = new Fibonacci();
            Console.WriteLine(fibonacci1.Display(5));
            Console.ReadKey();
        }
    }
    class Fibonacci
    {
        public int Display(int n)
        {
            if (n == 1 || n == 2) return 1;
            else
            return (Display(n - 1) + Display(n - 2));
        }
    }
}
