﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad3._5
{
    class Program
    {
        static void Main(string[] args)
        {
            double a, b;
            Console.WriteLine("Program oblicza dlugosc odcinka, podaj dwie liczby");
            a = Convert.ToDouble(Console.ReadLine());
            b = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine(Dlugosc(a, b));
            Console.ReadKey();
        }
        public static double Dlugosc(double x, double y)
        {
            return Math.Abs(y - x);
        }
    }
}
