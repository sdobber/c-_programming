﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad3._14
{
    class Program
    {
        static void Main(string[] args)
        {
            Trojkat trojkat1 = new Trojkat(4, 6);
            trojkat1.Display();
            Console.ReadKey();
        }
    }
    internal class Trojkat
    {
        private int a, h;
        public Trojkat(int a_, int h_)
        {
            a = a_;
            h = h_;
        }

        public void Display()
        {
            Console.WriteLine("{0} {1} {2}", a, h, a*h/2);
        }
    }
}
