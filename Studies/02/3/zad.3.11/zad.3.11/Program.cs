﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad._3._11
{
    class Program
    {
        static void Main(string[] args)
        {
            Suma suma1 = new Suma();
            Console.WriteLine(suma1.Display(10));
            Console.ReadKey();
        }
    }
    class Suma
    {
        public int Display(int n)
        {
            if (n == 1) return 3 * n + 1;
            else
                return 3*n+1 + Display(n-1);
        }
    }
}
