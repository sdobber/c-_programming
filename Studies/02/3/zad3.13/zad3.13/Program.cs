﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad3._13
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, b;
            Console.WriteLine("Podaj pierwszy bok prostokatu");
            a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Podaj drugi bok prostokatu");
            b = Convert.ToInt32(Console.ReadLine());
            Prostokat prostokat1 = new Prostokat(a, b);
            prostokat1.Display();
            Console.ReadKey();
        }
        private class Prostokat
        {
            private int a, h;
            public Prostokat(int a_, int h_)
            {
                a = a_;
                h = h_;
            }
            public void Display()
            {
                Console.WriteLine("{0} {1} {2}", a, h, a*h);
            }
        }
    }

}
