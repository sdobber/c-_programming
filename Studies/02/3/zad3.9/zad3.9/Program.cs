﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad3._9
{
    class Program
    {
        static void Main(string[] args)
        {
            Kwadrat kwadrat1 = new Kwadrat();
            kwadrat1.DisplaySquare();
            Console.ReadKey();
        }
        
    }
    class Kwadrat
    {
        Punkt A = new Punkt(1.2, 3.7);
        Punkt B = new Punkt(4.5, 3.7);
        Punkt C = new Punkt(4.5, 7.0);
        Punkt D = new Punkt(1.2, 7.0);

        public void DisplaySquare()
        {
            A.DisplayPoint();
            B.DisplayPoint();
            C.DisplayPoint();
            D.DisplayPoint();
        }
    }
    struct Punkt
    {
        private double x, y;
        public Punkt(double x_, double y_)
        {
            x = x_;
            y = y_;
        }
        public void DisplayPoint()
        {
            Console.WriteLine("{0} {1}", x, y);
        }
    }
}
