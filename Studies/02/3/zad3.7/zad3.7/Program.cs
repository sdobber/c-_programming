﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad3._7
{
    class Program
    {
        static void Main(string[] args)
        {
            Samochod samochod1 = new Samochod();
            Samochod samochod2 = new Samochod();
            Samochod samochod3 = new Samochod();
            samochod1.GetValues("czerwony", "toyota", 1990, 1.2, 23000);
            samochod2.GetValues("zielony", "bmw", 1991, 2.1, 25600);
            samochod3.GetValues("czarny", "mercedes", 2000, 3.6, 53000);
            samochod1.DisplayValues();
            samochod2.DisplayValues();
            samochod3.DisplayValues();
            Console.ReadKey();
        }
        struct Samochod
        {
            private string kolor, marka;
            private int rocznik, przebieg;
            private double pojemnosc;
            public void GetValues(string kolor_, string marka_, int rocznik_, double pojemnosc_, int przebieg_)
            {
                kolor = kolor_;
                marka = marka_;
                rocznik = rocznik_;
                pojemnosc = pojemnosc_;
                przebieg = przebieg_;
            }
            public void DisplayValues()
            {
                Console.WriteLine("{0} {1} {2} {3} {4}", kolor, marka, rocznik, pojemnosc, przebieg);
            }
        }
    }
}
