﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad3._3
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Console.WriteLine("Program liczy delte rownania dla 0, 1, 2 lub 3 argumentow");

            string reading = Console.ReadLine();
            string[] splitting = reading.Split(',');
            if (reading=="")
            {
                Console.WriteLine(Liczenie.Delta());
            }
            else
            {
                if (splitting.Length == 1)
                {
                    Console.WriteLine(Liczenie.Delta(splitting[0]));
                }
                if (splitting.Length == 2)
                {
                    Console.WriteLine(Liczenie.Delta(splitting[0], splitting[1]));
                }
                if (splitting.Length == 3)
                {
                    Console.WriteLine(Liczenie.Delta(splitting[0], splitting[1], splitting[2]));
                }
            }
            

            Console.ReadKey();
        }
    }

    class Liczenie
    {
        public static string Delta()
        {
            return "Nie podano argumentow";
        }

        public static string Delta(string x)
        {
            return "Delta jest rowna 0";
        }

        public static double Delta(string x, string y)
        {
            double a;
            a = Convert.ToDouble(y);
            return a * a;
        }

        public static double Delta(string x, string y, string z)
        {
            double a, b, c;
            a = Convert.ToDouble(x);
            b = Convert.ToDouble(y);
            c = Convert.ToDouble(z);
            return b * b - 4 * a * c;
        }
    }
}
