﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad6._3
{
    class Program
    {
        static void Main(string[] args)
        {
            Hashtable hash1 = new Hashtable();
            hash1[1] = "One";
            hash1[2] = "Two";
            hash1[3] = "Three";
            foreach (DictionaryEntry i in hash1)
            {
                Console.WriteLine("{0} {1}", i.Key, i.Value);
            }
            Console.ReadKey();
        }
    }
}
