﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad6._6
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue<char> queue1 = new Queue<char>();

            queue1.Enqueue('O');
            queue1.Enqueue('B');
            queue1.Enqueue('I');
            queue1.Enqueue('E');
            queue1.Enqueue('K');
            queue1.Enqueue('T');
            queue1.Enqueue('O');
            queue1.Enqueue('W');
            queue1.Enqueue('Y');

            foreach (var i in queue1)
            {
                Console.Write("{0} ", i);
            }
            Console.WriteLine();
            Console.WriteLine(queue1.Dequeue());
            Console.WriteLine();
            foreach (var i in queue1)
            {
                Console.Write("{0} ", i);
            }
            Console.ReadKey();
        }
    }
}
