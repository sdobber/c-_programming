﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad6._5
{
    class Program
    {
        static void Main(string[] args)
        {
            Random number = new Random();
            Stack<int> stos1 = new Stack<int>();

            for(int i=0; i<7; i++)
            {
                stos1.Push(number.Next(1, 11));
            }
            foreach (var i in stos1)
            {
                Console.Write("{0} ", i);
            }
            stos1.Pop();
            Console.WriteLine();
            Console.WriteLine(stos1.Peek());
            Console.ReadKey();
        }
    }
}
