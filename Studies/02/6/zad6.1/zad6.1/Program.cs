﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad6._1
{
    class Program
    {
        static void Main(string[] args)
        {
            Random number = new Random();
            ArrayList lista1 = new ArrayList();
            for(int i = 0; i<7; i++)
            {
                lista1.Add(number.Next(1, 11));
            }
            foreach (var i in lista1)
            {
                Console.Write("{0} ", i);
            }
            lista1.Sort();
            Console.WriteLine();
            foreach (var i in lista1)
            {
                Console.Write("{0} ", i);
            }
            Console.ReadKey();
        }
    }
}
