﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad6._2
{
    class Program
    {
        static void Main(string[] args)
        {
            ArrayList lista1 = new ArrayList();
            for(int i=0; i<5; i++)
            {
                Console.WriteLine("Podaj liczbe. Zostanie dodana do listy.");
                lista1.Add(Convert.ToInt32(Console.ReadLine()));
            }
            Console.Clear();
            foreach (var i in lista1)
            {
                Console.Write("{0} ", i);
            }
            Console.WriteLine();
            lista1.RemoveAt(2);
            foreach (var i in lista1)
            {
                Console.Write("{0} ", i);
            }
            Console.WriteLine();
            lista1.Sort();
            foreach (var i in lista1)
            {
                Console.Write("{0} ", i);
            }
            Console.WriteLine();
            lista1.Reverse();
            foreach (var i in lista1)
            {
                Console.Write("{0} ", i);
            }
            Console.ReadKey();
        }
    }
}
