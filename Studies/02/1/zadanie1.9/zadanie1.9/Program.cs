﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadanie1._9
{
    class Program
    {
        static void Main(string[] args)
        {
            int n;
            Console.WriteLine("Podaj rozmiar tabliczki mnozenia");
            n = Convert.ToInt32(Console.ReadLine());
            for (int i = 0; i<n+1; i++)
            {
                Console.Write(" {0}\t",i);
                
            }
            Console.WriteLine();
            for (int i = 1; i <= n; i++)
            {
                Console.Write(" {0}\t", i);
                for (int j = 1; j <= n; j++)
                {
                    Console.Write(" {0}\t", i*j);
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }
    }
}
