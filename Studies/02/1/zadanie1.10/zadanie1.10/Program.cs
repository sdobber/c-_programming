﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadanie1._10
{
    class Program
    {
        static void Main(string[] args)
        {
            int n, a=1, b;
            Console.WriteLine("Podaj n, program poda n kolejnych wyrazow ciagu 3n+1");
            n = Convert.ToInt32(Console.ReadLine());
            while (a <= n)
            {
                b = (3 * a) + 1;
                Console.WriteLine(b);
                a++;
            }
            Console.ReadKey();
        }
    }
}
