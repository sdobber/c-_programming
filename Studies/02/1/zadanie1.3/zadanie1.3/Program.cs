﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadanie1._3
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, b;
            Console.WriteLine("Podaj dwie liczby");
            a = Convert.ToInt32(Console.ReadLine());
            b = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Suma {0} i {1} jest rowna {2}", a, b, a + b);
            Console.WriteLine("Roznica {0} i {1} jest rowna {2}", a, b, a - b);
            Console.WriteLine("Iloczyn {0} i {1} jest rowny {2}", a, b, a * b);
            Console.WriteLine("Iloraz {0} i {1} jest rowny {2}", a, b, a / b);
            Console.ReadKey();
        }
    }
}
