﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadanie1._6
{
    class Program
    {
        static void Main(string[] args)
        {
            int a;
            Console.WriteLine("Podaj rok, program sprawdzi czy jest przestepny czy nie");
            a = Convert.ToInt32(Console.ReadLine());
            if ((a % 4 == 0 && a % 100 != 0) || a % 400 == 0)
            {
                Console.WriteLine("Rok {0} jest przestepny", a);
            }
            else
            {
                Console.WriteLine("Rok {0} nie jest przestepny", a);
            }
            
        }
    }
}
