﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadanie1._8
{
    class Program
    {
        static void Main(string[] args)
        {
            int n, suma=0;
            Console.WriteLine("Program obliczy sume n kolejnych liczb naturalnych");
            Console.WriteLine("Podaj ilosc liczb");
            n = Convert.ToInt32(Console.ReadLine());
            for (int i = 0; i<=n; i++)
            {
                suma = suma + i;
            }
            Console.WriteLine("Suma jest rowna {0}", suma);
            Console.ReadKey();
        }
    }
}
