﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad4._1
{
    class Program
    {
        static void Main(string[] args)
        {
            Samochod samochod1 = new Samochod();
            samochod1.Marka = "Fiat";
            samochod1.Model = "Punto";
            samochod1.Pojemnosc = 1.1;
            Console.WriteLine(samochod1.Marka);
            Console.WriteLine(samochod1.Model);
            Console.WriteLine(samochod1.Pojemnosc);
            Console.ReadKey();
        }
    }
    class Samochod
    {
        private string marka, model;
        private double pojemnosc;

        public string Model
        {
            get
            {
                return model;
            }
            set
            {
                model = value;
            }
        }
        public string Marka
        {
            get
            {
                return marka;
            }
            set
            {
                marka = value;
            }
        }
        public double Pojemnosc
        {
            get
            {
                return pojemnosc;
            }
            set
            {
                pojemnosc = value;
            }
        }
    }
}
