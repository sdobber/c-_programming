﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad4._2
{
    class Program
    {
        static void Main(string[] args)
        {
            Czlowiek czlowiek1 = new Czlowiek();
            czlowiek1.Imie = "Frodo";
            czlowiek1.Nazwisko = "Baggins";
            Console.WriteLine(czlowiek1.Imie);
            Console.WriteLine(czlowiek1.Nazwisko);
            Console.ReadKey();
        }
    }
    class Czlowiek
    {
        private string imie, nazwisko;
        public string Imie
        {
            get
            {
                return imie;
            }
            set
            {
                imie = value;
            }
        }

        public string Nazwisko
        {
            get
            {
                return nazwisko;
            }
            set
            {
                nazwisko = value;
            }
        }
    }
}
