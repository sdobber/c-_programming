﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad4._9
{
    class Program
    {
        static void Main(string[] args)
        {
            Dane dane1 = new Dane();
            Dane dane2 = new Dane(7, 8);

            Console.WriteLine( dane1.ToString() );
            Console.WriteLine( dane2.ToString() );
            Console.ReadKey();
        }
    }

    class Dane
    {
        private int x, y;

        public Dane()
        {
            x = 4;
            y = 5;
        }

        public Dane(int x_, int y_)
        {
            x = x_;
            y = y_;
        }
        public override string ToString()
        {
            return "<" + x + "," + y+">";
        }
    }
}
