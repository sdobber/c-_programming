﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad4._4
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 0, b = 5;
            try
            {
                Console.WriteLine("{0}", b / a);
            }
            catch (DivideByZeroException e)
            {
                Console.WriteLine(e.Message);
            }
            Console.ReadKey();
        }
    }
}
