﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad4._8
{
    class Program
    {
        static void Main(string[] args)
        {
            Figura figura1 = new Figura();
            Figura figura2 = new Figura(5);
            Figura figura3 = new Figura(3, 7);

            figura1.DisplayField();
            figura2.DisplayField();
            figura3.DisplayField();
            Console.ReadKey();
        }
    }

    class Figura
    {
        private int x, y;

        public Figura()
        {
            x = 7;
            y = 5;
        }
        public Figura(int x_)
        {
            x = x_;
            y = x;
        }
        public Figura(int x_, int y_)
        {
            x = x_;
            y = y_;
        }
        public void DisplayField()
        {
            Console.WriteLine("{0}", x*y);
        }
    }
}
