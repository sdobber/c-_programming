﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad4._3
{
    class Program
    {
        static void Main(string[] args)
        {
            Towar towar1 = new Towar();
            towar1.id = 1;
            towar1.nazwa = "Jablko";
            towar1.waga = 2.5;
            Console.WriteLine("{0} {1} {2}", towar1.id, towar1.nazwa, towar1.waga);
            Console.ReadKey();
        }
    }
    class Towar
    {
        public int id;
        public string nazwa;
        public double waga;
    }
}
