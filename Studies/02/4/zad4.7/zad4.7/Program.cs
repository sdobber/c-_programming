﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad4._7
{
    class Program
    {
        static void Main(string[] args)
        {
            Punkt punkt1 = new Punkt();
            Punkt punkt2 = new Punkt(5);
            Punkt punkt3 = new Punkt(punkt2);

            punkt1.Display();
            punkt2.Display();
            punkt3.Display();
            Console.ReadKey();
        }
    }
    class Punkt
    {
        private int x;

        public Punkt()
        {
            x = 1;
        }
        public Punkt(int x_)
        {
            x = x_;
        }
        public Punkt(Punkt x_)
        {
            x = x_.x;
        }
        public void Display()
        {
            Console.WriteLine("{0}", x);
        }
    }
}
