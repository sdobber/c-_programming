﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad4._6
{
    class Program
    {
        static void Main(string[] args)
        {
            Dane dane1 = new Dane();
            dane1.Display();
            Console.ReadKey();
        }
    }
    class Dane
    {
        private int x, y;
        public Dane()
        {
            x = 55;
            y = 22;
        }
        public void Display()
        {
            Console.WriteLine("{0} {1}", x, y);
        }
    }
}
