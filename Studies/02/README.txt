Simple to somewhat advanced exercises.

1. User input, if (else), for, while, array, foreach, Random.

2. Static methods, simple Math methods, recursion, List.

3. Classes, more simple Math methods, method overloading, constructor, struct.

4. Get set, access modifiers, try-catch, constructor overloading, override.

5. Inheritance, abstract, protected, base, interface.

6. ArrayList, Hashtable, Stack, Queue, SortedList.