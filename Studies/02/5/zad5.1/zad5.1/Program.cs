﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad5._1
{
    class Program
    {
        static void Main(string[] args)
        {
            Druga druga1 = new Druga();
            druga1.A = 7;
            druga1.B = 8;
            druga1.C = 9;
            druga1.Display();
            Console.ReadKey();
        }
    }
    class Pierwsza
    {
        protected int a, b;
        public int A
        {
            get
            {
                return a;
            }
            set
            {
                a = value;
            }
        }
        public int B
        {
            get
            {
                return b;
            }
            set
            {
                b = value;
            }
        }
    }
    class Druga : Pierwsza
    {
        private int c;
        public int C
        {
            get
            {
                return c;
            }
            set
            {
                c = value;
            }
        }
        public void Display()
        {
            Console.WriteLine("{0} {1} {2} {3}", a, b, c, a+b+C);
        }
    }
}
