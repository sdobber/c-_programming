﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad5._5
{
    class Program
    {
        static void Main(string[] args)
        {
            Rower rower1 = new Rower();
            Tramwaj tramwaj1 = new Tramwaj();
            Samochod samochod1 = new Samochod();
            rower1.Kolor = "zielony";
            rower1.Cena = 1000.00;
            tramwaj1.Kolor = "czerwony";
            tramwaj1.Waga = 59000.00;
            samochod1.Kolor = "niebieski";
            samochod1.Predkosc = 360.00;
            rower1.Display();
            tramwaj1.Display();
            samochod1.Display();
            Console.ReadKey();
        }
    }
    abstract class Pojazd
    {
        protected string kolor;
        public string Kolor
        {
            get
            {
                return kolor;
            }
            set
            {
                kolor = value;
            }
        }
        public abstract void Display();
    }
    class Rower : Pojazd
    {
        private double cena;
        public double Cena
        {
            get
            {
                return cena;
            }
            set
            {
                cena = value;
            }
        }
        public override void Display()
        {
            Console.WriteLine("{0} {1}", kolor, cena);
        }
    }
    class Tramwaj : Pojazd
    {
        private double waga;
        public double Waga
        {
            get
            {
                return waga;
            }
            set
            {
                waga = value;
            }
        }
        public override void Display()
        {
            Console.WriteLine("{0} {1}", kolor, waga);
        }
    }
    class Samochod : Pojazd
    {
        private double predkosc;
        public double Predkosc
        {
            get
            {
                return predkosc;
            }
            set
            {
                predkosc = value;
            }
        }
        public override void Display()
        {
            Console.WriteLine("{0} {1}", kolor, predkosc);
        }
    }
}
