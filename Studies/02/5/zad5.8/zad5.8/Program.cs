﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad5._8
{
    public interface KwadratSumy
    {
        int Licz(int a, int b);
    }
    class Program
    {
        static void Main(string[] args)
        {
            int a, b;
            Nowa nowa1 = new Nowa();
            Console.WriteLine("Podaj pierwsza liczbe");
            a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Podaj druga liczbe");
            b = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine(nowa1.Licz(a,b));

            Console.ReadKey();
        }
    }
    class Nowa : KwadratSumy
    {
        public int Licz(int a, int b)
        {
            return (a + b) * (a + b);
        }
    }
}
