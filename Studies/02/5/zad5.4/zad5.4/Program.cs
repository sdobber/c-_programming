﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad5._4
{
    class Program
    {
        static void Main(string[] args)
        {
            Dziecko dziecko1 = new Dziecko();
            dziecko1.KolorOczu = "zielone";
            dziecko1.Imie = "Doom";
            dziecko1.Display();
            Console.ReadKey();
        }
    }
    class Rodzice
    {
        protected string kolorOczu;
        public string KolorOczu
        {
            get
            {
                return kolorOczu;
            }
            set
            {
                kolorOczu = value;
            }
        }
    }
    class Dziecko : Rodzice
    {
        private string imie;
        public string Imie
        {
            get
            {
                return imie;
            }
            set
            {
                imie = value;
            }
        }
        public void Display()
        {
            Console.WriteLine("{0} {1}", kolorOczu, imie);
        }
    }
}
