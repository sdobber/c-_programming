﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad5._2
{
    class Program
    {
        static void Main(string[] args)
        {
            Lekarz lekarz1 = new Lekarz("Cezary", "Cezary", "mezczyzna", 100);
            Kominiarz kominiarz1 = new Kominiarz("Pan", "Kleks", "mezczyzna", "ogromne");
            Budowlaniec budowlaniec1 = new Budowlaniec("Bob", "Budowniczy", "mezczyzna", 4700.0);
            lekarz1.Display();
            kominiarz1.Display();
            budowlaniec1.Display();
            Console.ReadKey();
        }
    }
    abstract class Czlowiek
    {
        protected string imie, nazwisko, plec;
        public abstract void Display();
    }
    class Lekarz : Czlowiek
    {
        private int gabinet;
        public Lekarz(string imie_, string nazwisko_, string plec_, int gabinet_)
        {
            imie = imie_;
            nazwisko = nazwisko_;
            plec = plec_;
            gabinet = gabinet_;
        }
        public override void Display()
        {
            Console.WriteLine("{0} {1} {2} {3}", imie, nazwisko, plec, gabinet);
        }

    }
    class Kominiarz : Czlowiek
    {
        private string uprawnienia;
        public Kominiarz (string imie_, string nazwisko_, string plec_, string uprawnienia_)
        {
            imie = imie_;
            nazwisko = nazwisko_;
            plec = plec_;
            uprawnienia = uprawnienia_;
        }
        public override void Display()
        {
            Console.WriteLine("{0} {1} {2} {3}", imie, nazwisko, plec, uprawnienia);
        }
    }
    class Budowlaniec : Czlowiek
    {
        private double zarobki;
        public Budowlaniec(string imie_, string nazwisko_, string plec_, double zarobki_)
        {
            imie = imie_;
            nazwisko = nazwisko_;
            plec = plec_;
            zarobki = zarobki_;
        }
        public override void Display()
        {
            Console.WriteLine("{0} {1} {2} {3}", imie, nazwisko, plec, zarobki);
        }
    }
}
