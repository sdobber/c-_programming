﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad5._3
{
    class Program
    {
        static void Main(string[] args)
        {
            Szkola szkola1 = new Szkola();
            FirmaSzkoleniowa firma1 = new FirmaSzkoleniowa();
            szkola1.Poziom = 1;
            szkola1.Nazwa = "Podstawowa";
            firma1.Poziom = 2;
            firma1.Cena = 1200.00;
            szkola1.Display();
            firma1.Display();
            Console.ReadKey();
        }
    }
    abstract class Edukacja
    {
        protected int poziom;
        public int Poziom
        {
            get
            {
                return poziom;
            }
            set
            {
                poziom = value;
            }
        }
        public abstract void Display();
    }
    class Szkola : Edukacja
    {
        private string nazwa;
        public string Nazwa
        {
            get
            {
                return nazwa;
            }
            set
            {
                nazwa = value;
            }
        }
        public override void Display()
        {
            Console.WriteLine("{0} {1}", poziom, nazwa);
        }
    }
    class FirmaSzkoleniowa : Edukacja
    {
        private double cena;
        public double Cena
        {
            get
            {
                return cena;
            }
            set
            {
                cena = value;
            }
        }
        public override void Display()
        {
            Console.WriteLine("{0} {1}", poziom, cena);
        }
    }
}
