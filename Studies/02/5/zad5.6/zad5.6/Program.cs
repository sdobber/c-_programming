﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad5._6
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Punkt3D punkt13D = new Punkt3D(3, 6, 7);
            punkt13D.Display();
            Console.ReadKey();
        }
    }
    class Punkt
    {
        protected int x, y; 
        public Punkt(int x_, int y_)
        {
            x = x_;
            y = y_;
        }
    }
    class Punkt3D : Punkt
    {
        private int z;
        public Punkt3D(int x_, int y_, int z_) : base (x_,y_)
        {
            x = x_;
            y = y_;
            z = z_;
        }
        public void Display()
        {
            Console.WriteLine("{0} {1} {2}", x, y, z);
        }
    }
}
