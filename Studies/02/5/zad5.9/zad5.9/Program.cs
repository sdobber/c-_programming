﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad5._9
{
    public interface Suma
    {
        int dodawanie(int a, int b);
    }
    public interface Roznica
    {
        int odejmowanie(int a, int b);
    }
    class Program
    {
        static void Main(string[] args)
        {
            int a, b;
            Dzialanie1 d1 = new Dzialanie1();
            Dzialanie2 d2 = new Dzialanie2();
            Console.WriteLine("Podaj pierwsza liczbe");
            a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Podaj druga liczbe");
            b = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(d1.dodawanie(a, b));
            Console.WriteLine(d2.odejmowanie(a, b));
            Console.ReadKey();

        }
    }
    class Dzialanie1 : Suma
    {
        public int dodawanie(int a, int b)
        {
            return a + b;
        }
    }
    class Dzialanie2 : Roznica
    {
        public int odejmowanie(int a, int b)
        {
            return a - b;
        }
    }

}
