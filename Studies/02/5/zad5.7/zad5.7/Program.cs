﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad5._7
{
    public interface Dzialania
    {
        int dodawanie(int a, int b);
        int odejmowanie(int a, int b);
        int mnozenie(int a, int b);
        double dzielenie(int a, int b);
        
    }
    class Program
    {
        static void Main(string[] args)
        {
            int a, b;
            Kalkulator kalkulator1 = new Kalkulator();
            Console.WriteLine("Podaj pierwsza liczbe");
            a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Podaj druga liczbe");
            b = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(kalkulator1.dodawanie(a, b));
            Console.WriteLine(kalkulator1.odejmowanie(a, b));
            Console.WriteLine(kalkulator1.mnozenie(a, b));
            Console.WriteLine(kalkulator1.dzielenie(a, b));
            Console.ReadKey();
        }
    }
    class Kalkulator : Dzialania
    {
        public int dodawanie(int a, int b)
        {
            return a + b;
        }

        public double dzielenie(int a, int b)
        {
            return (double)a / b;
        }

        public int mnozenie(int a, int b)
        {
            return a * b;
        }

        public int odejmowanie(int a, int b)
        {
            return a - b;
        }
    }
}
