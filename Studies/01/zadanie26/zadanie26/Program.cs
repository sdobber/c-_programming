﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadanie26
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Podaj liczbe");
            int a = Convert.ToInt32(Console.ReadLine());
            int suma = 0;
            int i = 1;
            do
            {
                if (i % 2 == 1)
                {
                    suma += i;
                }
                i++;
            } while (i <= a);
            Console.WriteLine("Suma liczb nieparzystych od 1 do {0} jet rowna {1}", a, suma);
            Console.ReadLine();
        }
    }
}
