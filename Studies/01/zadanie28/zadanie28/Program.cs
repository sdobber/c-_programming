﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadanie28
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Podaj trzy liczby");
            int a = Convert.ToInt32(Console.ReadLine());
            int b = Convert.ToInt32(Console.ReadLine());
            int c = Convert.ToInt32(Console.ReadLine());
            int min = a;
            if (b < min)
            {
                min = b;
                if (c < min)
                {
                    min = c;
                }
            }
            Console.WriteLine("Najmniejsza liczba to {0}", min);
            Console.ReadLine();
        }
    }
}
