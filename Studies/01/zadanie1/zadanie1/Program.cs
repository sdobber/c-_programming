﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadanie1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Podaj liczbe calkowita.");
            int a;
            a = Convert.ToInt32(Console.ReadLine());
            if (a < 0)
            {
                Console.WriteLine("Liczba jest ujemna");
            }
            else
            {
                if (a == 0)
                {
                    Console.WriteLine("Liczba jest rowna 0");
                }
                else
                {
                    Console.WriteLine("Liczba jest dodatnia");
                }
            }
            Console.ReadLine();
        }
    }
}
