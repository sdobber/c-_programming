﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadanie23
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Podaj liczbe");
            int a = Convert.ToInt32(Console.ReadLine());
            int n=1;
            int suma = 0;
            do
            {
                suma += n;
                n++;
            } while (n <= a);
            Console.WriteLine("Suma od 1 do {0} jest rowna {1}",a,suma);
            Console.ReadLine();
        }
    }
}
