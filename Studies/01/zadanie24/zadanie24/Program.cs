﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadanie24
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Podaj liczbe");
            int a = Convert.ToInt32(Console.ReadLine());
            int suma = 0;
            for (int i = 1; i <= a; i++)
            {
                if (i % 2 == 1)
                {
                    suma += i;
                }
                
            }
            Console.WriteLine("Suma liczb nieparzystych od 1 do {0} jest rowna {1}", a, suma);
            Console.ReadLine();
        }
    }
}
