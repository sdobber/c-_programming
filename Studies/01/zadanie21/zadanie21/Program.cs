﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadanie21
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("podaj liczbe");
            int a = Convert.ToInt32(Console.ReadLine());
            int suma = 0;
            for (int i = 1; i <= a; i++)
            {
                suma += i;
            }
            Console.WriteLine("Suma liczb od 1 do {1} jest rowna {0}",suma,a);
            Console.ReadLine();
        }
    }
}
