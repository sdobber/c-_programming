﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeCheck
{
    class Program
    {
        static void Main(string[] args)
        {
            string a, b, c;
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            using (StreamWriter writer = new StreamWriter("C:\\tmp\\log.txt", true))
            {
                for (int i = 0; i < 100000; i++)
                {
                    writer.WriteLine(i);
                }
            }
            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:000}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds);
            a = "Run time" + elapsedTime;
            stopWatch.Reset();
            stopWatch.Start();
            for (int i = 0; i < 100000; i++)
            {
                Console.WriteLine(i);
            }
            stopWatch.Stop();
            ts = stopWatch.Elapsed;
            elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:000}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds);
            b = "Run time" + elapsedTime;
            stopWatch.Reset();
            stopWatch.Start();
            for (int i = 0; i < 100000; i++) ;
            stopWatch.Stop();
            ts = stopWatch.Elapsed;
            elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:000}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds);
            c = "Run time" + elapsedTime;
            Console.WriteLine();
            Console.WriteLine(a + "\n" + b + "\n" + c);
            Console.ReadKey();
        }
    }
}
