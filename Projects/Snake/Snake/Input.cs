﻿using System.Collections;
using System.Windows.Forms;

namespace Snake
{
    class Input
    {
        //load a list of keyboard buttons
        private static Hashtable keyTable = new Hashtable();

        //check if a particular button is pressed
        public static bool KeyPressed (Keys key)
        {
            if (keyTable[key] == null)
            {
                return false;
            }

            return (bool)keyTable[key];
        }

        //detect if a keyboard button is pressed
        public static void ChangeState (Keys key, bool state)
        {
            keyTable[key] = state;
        }
    }
}
