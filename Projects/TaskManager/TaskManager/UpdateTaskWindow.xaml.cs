﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TaskManager
{
    /// <summary>
    /// Interaction logic for UpdateTaskWindow.xaml
    /// </summary>
    public partial class UpdateTaskWindow : Window
    {
        TaskManagerDBEntities3 db;
        MainWindow mw = (MainWindow)Application.Current.MainWindow;
        Regex regexObj = new Regex(@"[^\d]");
        public UpdateTaskWindow()
        {
            InitializeComponent();
        }

        //method used to update currently selected task
        private void UpdateCurrentTask(object sender, RoutedEventArgs e)
        {
            bool toDo=false, inProgress=false, done=false;
            if (StatusComboBox.Text == "To do")
            {
                toDo = true;
                inProgress = false;
                done = false;
            }
            if (StatusComboBox.Text == "In progress")
            {
                toDo = false;
                inProgress = true;
                done = false;
            }
            if (StatusComboBox.Text == "Done")
            {
                toDo = false;
                inProgress = false;
                done = true;
            }
            using (db = new TaskManagerDBEntities3())
            {
                
                string currentIDstring = regexObj.Replace((string)IDLabel.Content, "");
                int currentID = Convert.ToInt32(currentIDstring);
                //UpdateTask is an SQL procedure that allows user to update specified ID task with current values of the fields
                db.UpdateTask(currentID, TitleTextBox.Text, ContentsTextBox.Text, PriorityComboBox.Text, StatusComboBox.Text, SubmissionDate.SelectedDate.Value.Date, toDo, inProgress, done);
            }

            mw.PopulateToDoList();
            mw.PopulateInProgressList();
            mw.PopulateDoneList();
            Close();
        }

        //method used to delete the task with specified ID
        private void DeleteCurrentTask(object sender, RoutedEventArgs e)
        {
            using (db = new TaskManagerDBEntities3())
            {
                //Regex used to remove all non-digit symbols from string
                string currentIDstring = regexObj.Replace((string)IDLabel.Content, "");
                int currentID = Convert.ToInt32(currentIDstring);
                //DeleteTask is an SQL procedure that allows user to delete a task with specified ID from the database
                db.DeleteTask(currentID);
            }

            mw.PopulateToDoList();
            mw.PopulateInProgressList();
            mw.PopulateDoneList();
            Close();
        }

        //method used to close current task
        private void CloseCurrentTask(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
