﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TaskManager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        TaskManagerDBEntities3 db;
        
        //lists used for sorting in ListViews
        List<ShowToDoTasks_Result> ToDoList = new List<ShowToDoTasks_Result>();
        List<ShowInProgressTasks_Result> InProgressList = new List<ShowInProgressTasks_Result>();
        List<ShowDoneTasks_Result> DoneList = new List<ShowDoneTasks_Result>();
        
        public MainWindow()
        {
            InitializeComponent();

            //displaying data in ListViews
            PopulateToDoList();
            PopulateInProgressList();
            PopulateDoneList();  
            
        }
        
        //method used to display data in To do listview
        public void PopulateToDoList()
        {
            ToDoListView.Items.Clear();
            ToDoList.Clear();
            using(db = new TaskManagerDBEntities3())
            {
                //ShowToDoTasks is an SQL procedure to select Tasks, from the database, whose ToDo flag is set to true
                var tasks = db.ShowToDoTasks();
                foreach (var item in tasks)
                {
                    ToDoListView.Items.Add(item);
                    ToDoList.Add(item);
                }
            }
        }
        //method used to display data in In progress listview
        public void PopulateInProgressList()
        {
            InProgressListView.Items.Clear();
            InProgressList.Clear();
            using (db = new TaskManagerDBEntities3())
            {
                //ShowInProgressTasks is an SQL procedure to select Tasks, from the database, whose InProgress flag is set to true
                var tasks = db.ShowInProgressTasks();
                foreach (var item in tasks)
                {
                    InProgressListView.Items.Add(item);
                    InProgressList.Add(item);
                }
            }
        }
        //method used to display data in Done listview
        public void PopulateDoneList()
        {
            DoneListView.Items.Clear();
            DoneList.Clear();
            using (db = new TaskManagerDBEntities3())
            {
                //ShowDoneTasks is an SQL procedure to select Tasks, from the database, whose Done flag is set to true
                var tasks = db.ShowDoneTasks();
                foreach (var item in tasks)
                {
                    DoneListView.Items.Add(item);
                    DoneList.Add(item);
                }
            }
        }
        //this method displays a new window where a user can create a new with specified values of fields
        private void CreateNewTask(object sender, RoutedEventArgs e)
        {
            
            NewTaskWindow newTaskWindow = new NewTaskWindow();
            newTaskWindow.Show();
        }

        //this method is used to display a new window that lets user update To do task's details
        private void ToDoListTaskUpdate(object sender, MouseButtonEventArgs e)
        {
            ListView item = sender as ListView;
            ShowToDoTasks_Result itemValue = (ShowToDoTasks_Result)item.SelectedValue;
            if (itemValue != null)
            {
                UpdateTaskWindow updateTaskWindow = new UpdateTaskWindow();
                //value of the details of a task in a new window is taken from selected item from To do list from main window
                updateTaskWindow.IDLabel.Content += " " + itemValue.ID;
                updateTaskWindow.TitleTextBox.Text = itemValue.Title;
                updateTaskWindow.ContentsTextBox.Text = itemValue.Contents;
                updateTaskWindow.PriorityComboBox.Text = itemValue.Priority;
                updateTaskWindow.StatusComboBox.Text = itemValue.TaskStatus;
                updateTaskWindow.SubmissionDate.SelectedDate = itemValue.SubmissionDate;
                updateTaskWindow.Show();
                ToDoListView.SelectedItem = null;
            }
        }

        //this method is used to display a new window that lets user update In progress task's details
        private void InProgressListTaskUpdate(object sender, MouseButtonEventArgs e)
        {
            ListView item = sender as ListView;
            ShowInProgressTasks_Result itemValue = (ShowInProgressTasks_Result)item.SelectedValue;
            if (itemValue != null)
            {
                UpdateTaskWindow updateTaskWindow = new UpdateTaskWindow();
                //value of the details of a task in a new window is taken from selected item from In progress list from main window
                updateTaskWindow.IDLabel.Content += " " + itemValue.ID;
                updateTaskWindow.TitleTextBox.Text = itemValue.Title;
                updateTaskWindow.ContentsTextBox.Text = itemValue.Contents;
                updateTaskWindow.PriorityComboBox.Text = itemValue.Priority;
                updateTaskWindow.StatusComboBox.Text = itemValue.TaskStatus;
                updateTaskWindow.SubmissionDate.SelectedDate = itemValue.SubmissionDate;
                updateTaskWindow.Show();
                InProgressListView.SelectedItem = null;
            }
        }

        //this method is used to display a new window that lets user update Done task's details
        private void DoneListTaskUpdate(object sender, MouseButtonEventArgs e)
        {
            ListView item = sender as ListView;
            ShowDoneTasks_Result itemValue = (ShowDoneTasks_Result)item.SelectedValue;
            if (itemValue != null)
            {
                UpdateTaskWindow updateTaskWindow = new UpdateTaskWindow();
                //value of the details of a task in a new window is taken from selected item from Done list from main window
                updateTaskWindow.IDLabel.Content += " " + itemValue.ID;
                updateTaskWindow.TitleTextBox.Text = itemValue.Title;
                updateTaskWindow.ContentsTextBox.Text = itemValue.Contents;
                updateTaskWindow.PriorityComboBox.Text = itemValue.Priority;
                updateTaskWindow.StatusComboBox.Text = itemValue.TaskStatus;
                updateTaskWindow.SubmissionDate.SelectedDate = itemValue.SubmissionDate;
                updateTaskWindow.Show();
                DoneListView.SelectedItem = null;
            }
        }

        //this method lets user delete all task from To do list
        private void ToDoListDeleteAllTasks(object sender, RoutedEventArgs e)
        {
            //displaying confirmation box preventing accidental deletion of tasks
            MessageBoxResult result = MessageBox.Show("Do you want to delete all tasks from this list?", "Delete all tasks?", MessageBoxButton.OKCancel, MessageBoxImage.Question);
            if (result == MessageBoxResult.OK)
            {
                foreach (ShowToDoTasks_Result item in ToDoListView.Items)
                {
                    using (db = new TaskManagerDBEntities3())
                    {
                        //DeleteTask is an SQL procedure that lets user delete the task with specified ID from the database
                        db.DeleteTask(item.ID);
                    }
                }
                //updating display of To do list
                PopulateToDoList();
            }
        }

        //this method lets user delete all task from To do list
        private void InProgressListDeleteAllTasks(object sender, RoutedEventArgs e)
        {
            //displaying confirmation box preventing accidental deletion of tasks
            MessageBoxResult result = MessageBox.Show("Do you want to delete all tasks from this list?", "Delete all tasks?", MessageBoxButton.OKCancel, MessageBoxImage.Question);
            if (result == MessageBoxResult.OK)
            {
                foreach (ShowInProgressTasks_Result item in InProgressListView.Items)
                {
                    using (db = new TaskManagerDBEntities3())
                    {
                        db.DeleteTask(item.ID);
                    }
                }
                //updating display of In progress list
                PopulateInProgressList();
            }
        }

        //this method lets user delete all task from Done list
        private void DoneListDeleteAllTasks(object sender, RoutedEventArgs e)
        {
            //displaying confirmation box preventing accidental deletion of tasks
            MessageBoxResult result = MessageBox.Show("Do you want to delete all tasks from this list?", "Delete all tasks?", MessageBoxButton.OKCancel, MessageBoxImage.Question);
            if (result == MessageBoxResult.OK)
            {
                foreach (ShowDoneTasks_Result item in DoneListView.Items)
                {
                    using (db = new TaskManagerDBEntities3())
                    {
                        db.DeleteTask(item.ID);
                    }
                }
                //updating display of Done list
                PopulateDoneList();
            }
        }
        
        //method used to sort To do list tasks
        private void ToDoListSort(object sender, EventArgs e)
        {
            ComboBox cb = sender as ComboBox;
            string currentlySelectedSortValue = cb.Text;
            switch (currentlySelectedSortValue)
            {
                case "ID a":
                    ToDoList = ToDoList.OrderBy(t => t.ID).ToList();
                    ToDoListView.Items.Clear();
                    foreach (var item in ToDoList)
                    {
                        ToDoListView.Items.Add(item);
                    }
                    break;
                case "ID d":
                    ToDoList = ToDoList.OrderByDescending(t => t.ID).ToList();
                    ToDoListView.Items.Clear();
                    foreach (var item in ToDoList)
                    {
                        ToDoListView.Items.Add(item);
                    }
                    break;
                case "Title a":
                    ToDoList = ToDoList.OrderBy(t => t.Title).ToList();
                    ToDoListView.Items.Clear();
                    foreach (var item in ToDoList)
                    {
                        ToDoListView.Items.Add(item);
                    }
                    break;
                case "Title d":
                    ToDoList = ToDoList.OrderByDescending(t => t.Title).ToList();
                    ToDoListView.Items.Clear();
                    foreach (var item in ToDoList)
                    {
                        ToDoListView.Items.Add(item);
                    }
                    break;
                case "Priority a":
                    ToDoList = ToDoList.OrderBy(t => t.Priority).ToList();
                    ToDoListView.Items.Clear();
                    foreach (var item in ToDoList)
                    {
                        ToDoListView.Items.Add(item);
                    }
                    break;
                case "Priority d":
                    ToDoList = ToDoList.OrderByDescending(t => t.Priority).ToList();
                    ToDoListView.Items.Clear();
                    foreach (var item in ToDoList)
                    {
                        ToDoListView.Items.Add(item);
                    }
                    break;
                case "Creation Date a":
                    ToDoList = ToDoList.OrderBy(t => t.CreationDate).ToList();
                    ToDoListView.Items.Clear();
                    foreach (var item in ToDoList)
                    {
                        ToDoListView.Items.Add(item);
                    }
                    break;
                case "Creation Date d":
                    ToDoList = ToDoList.OrderByDescending(t => t.CreationDate).ToList();
                    ToDoListView.Items.Clear();
                    foreach (var item in ToDoList)
                    {
                        ToDoListView.Items.Add(item);
                    }
                    break;
                case "Sumission Date a":
                    ToDoList = ToDoList.OrderBy(t => t.SubmissionDate).ToList();
                    ToDoListView.Items.Clear();
                    foreach (var item in ToDoList)
                    {
                        ToDoListView.Items.Add(item);
                    }
                    break;
                case "Submission Date d":
                    ToDoList = ToDoList.OrderByDescending(t => t.SubmissionDate).ToList();
                    ToDoListView.Items.Clear();
                    foreach (var item in ToDoList)
                    {
                        ToDoListView.Items.Add(item);
                    }
                    break;
                default:
                    break;
            }
        }

        //method used to sort In progress list tasks
        private void InProgressListSort(object sender, EventArgs e)
        {
            ComboBox cb = sender as ComboBox;
            string currentlySelectedSortValue = cb.Text;
            switch (currentlySelectedSortValue)
            {
                case "ID a":
                    InProgressList = InProgressList.OrderBy(t => t.ID).ToList();
                    InProgressListView.Items.Clear();
                    foreach (var item in InProgressList)
                    {
                        InProgressListView.Items.Add(item);
                    }
                    break;
                case "ID d":
                    InProgressList = InProgressList.OrderByDescending(t => t.ID).ToList();
                    InProgressListView.Items.Clear();
                    foreach (var item in InProgressList)
                    {
                        InProgressListView.Items.Add(item);
                    }
                    break;
                case "Title a":
                    InProgressList = InProgressList.OrderBy(t => t.Title).ToList();
                    InProgressListView.Items.Clear();
                    foreach (var item in InProgressList)
                    {
                        InProgressListView.Items.Add(item);
                    }
                    break;
                case "Title d":
                    InProgressList = InProgressList.OrderByDescending(t => t.Title).ToList();
                    InProgressListView.Items.Clear();
                    foreach (var item in InProgressList)
                    {
                        InProgressListView.Items.Add(item);
                    }
                    break;
                case "Priority a":
                    InProgressList = InProgressList.OrderBy(t => t.Priority).ToList();
                    InProgressListView.Items.Clear();
                    foreach (var item in InProgressList)
                    {
                        InProgressListView.Items.Add(item);
                    }
                    break;
                case "Priority d":
                    InProgressList = InProgressList.OrderByDescending(t => t.Priority).ToList();
                    InProgressListView.Items.Clear();
                    foreach (var item in InProgressList)
                    {
                        InProgressListView.Items.Add(item);
                    }
                    break;
                case "Creation Date a":
                    InProgressList = InProgressList.OrderBy(t => t.CreationDate).ToList();
                    InProgressListView.Items.Clear();
                    foreach (var item in InProgressList)
                    {
                        InProgressListView.Items.Add(item);
                    }
                    break;
                case "Creation Date d":
                    InProgressList = InProgressList.OrderByDescending(t => t.CreationDate).ToList();
                    InProgressListView.Items.Clear();
                    foreach (var item in InProgressList)
                    {
                        InProgressListView.Items.Add(item);
                    }
                    break;
                case "Sumission Date a":
                    InProgressList = InProgressList.OrderBy(t => t.SubmissionDate).ToList();
                    InProgressListView.Items.Clear();
                    foreach (var item in InProgressList)
                    {
                        InProgressListView.Items.Add(item);
                    }
                    break;
                case "Submission Date d":
                    InProgressList = InProgressList.OrderByDescending(t => t.SubmissionDate).ToList();
                    InProgressListView.Items.Clear();
                    foreach (var item in InProgressList)
                    {
                        InProgressListView.Items.Add(item);
                    }
                    break;
                default:
                    break;
            }
        }

        //method used to sort Done list tasks
        private void DoneListSort(object sender, EventArgs e)
        {
            ComboBox cb = sender as ComboBox;
            string currentlySelectedSortValue = cb.Text;
            switch (currentlySelectedSortValue)
            {
                case "ID a":
                    DoneList = DoneList.OrderBy(t => t.ID).ToList();
                    DoneListView.Items.Clear();
                    foreach (var item in DoneList)
                    {
                        DoneListView.Items.Add(item);
                    }
                    break;
                case "ID d":
                    DoneList = DoneList.OrderByDescending(t => t.ID).ToList();
                    DoneListView.Items.Clear();
                    foreach (var item in DoneList)
                    {
                        DoneListView.Items.Add(item);
                    }
                    break;
                case "Title a":
                    DoneList = DoneList.OrderBy(t => t.Title).ToList();
                    DoneListView.Items.Clear();
                    foreach (var item in DoneList)
                    {
                        DoneListView.Items.Add(item);
                    }
                    break;
                case "Title d":
                    DoneList = DoneList.OrderByDescending(t => t.Title).ToList();
                    DoneListView.Items.Clear();
                    foreach (var item in DoneList)
                    {
                        DoneListView.Items.Add(item);
                    }
                    break;
                case "Priority a":
                    DoneList = DoneList.OrderBy(t => t.Priority).ToList();
                    DoneListView.Items.Clear();
                    foreach (var item in DoneList)
                    {
                        DoneListView.Items.Add(item);
                    }
                    break;
                case "Priority d":
                    DoneList = DoneList.OrderByDescending(t => t.Priority).ToList();
                    DoneListView.Items.Clear();
                    foreach (var item in DoneList)
                    {
                        DoneListView.Items.Add(item);
                    }
                    break;
                case "Creation Date a":
                    DoneList = DoneList.OrderBy(t => t.CreationDate).ToList();
                    DoneListView.Items.Clear();
                    foreach (var item in DoneList)
                    {
                        DoneListView.Items.Add(item);
                    }
                    break;
                case "Creation Date d":
                    DoneList = DoneList.OrderByDescending(t => t.CreationDate).ToList();
                    DoneListView.Items.Clear();
                    foreach (var item in DoneList)
                    {
                        DoneListView.Items.Add(item);
                    }
                    break;
                case "Sumission Date a":
                    DoneList = DoneList.OrderBy(t => t.SubmissionDate).ToList();
                    DoneListView.Items.Clear();
                    foreach (var item in DoneList)
                    {
                        DoneListView.Items.Add(item);
                    }
                    break;
                case "Submission Date d":
                    DoneList = DoneList.OrderByDescending(t => t.SubmissionDate).ToList();
                    DoneListView.Items.Clear();
                    foreach (var item in DoneList)
                    {
                        DoneListView.Items.Add(item);
                    }
                    break;
                default:
                    break;
            }
        }





    }
}
