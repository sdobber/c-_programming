﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TaskManager
{
    /// <summary>
    /// Interaction logic for NewTaskWindow.xaml
    /// </summary>
    public partial class NewTaskWindow : Window
    {
        TaskManagerDBEntities3 db;

        public NewTaskWindow()
        {
            InitializeComponent();
        }

        //this method allows user to save a task with currently specified fields' values
        private void SaveNewTask(object sender, RoutedEventArgs e)
        {
            MainWindow mw = (MainWindow)Application.Current.MainWindow;
            
            //AddTasks is an SQL procedure that adds a new task with specified values to the database
            using(db = new TaskManagerDBEntities3())
            {
                db.AddTask(TitleTextBox.Text, ContentsTextBox.Text, PriorityComboBox.Text, StatusComboBox.Text, DateTime.Now.Date, SubmissionDate.SelectedDate.Value.Date, true, false, false, false);
            }
            
            mw.PopulateToDoList();
            Close();
        }

        //method used to clear fields of a new task
        private void CancelNewTask(object sender, RoutedEventArgs e)
        {
            TitleTextBox.Clear();
            ContentsTextBox.Clear();
            PriorityComboBox.SelectedValue = Normal;
            StatusComboBox.SelectedValue = ToDo;
            SubmissionDate.SelectedDate = null;

        }

        //method used to close the new task window
        private void CloseNewTask(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
