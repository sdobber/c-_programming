Application whose purpose is to manage tasks.

Comprises a new task button and three ListViews: To do, In progress, Done.

A user can create a new task with the "New Task" button. It takes them to a new task window where they can fill in the details they want to create the new task with. They can do so by clicking the "Save" button. Should the user wish to clear the input fields they can simply click the "Cancel" button. "Close" button closes the "New Task" window.

When the user creates the new task, it will be displayed in one of the lists, according to the status of the task.

The user can edit the task by clicking it on the list. The user is presented then with the "Update Task" window, where they can update the task by clicking the "Update" button, delete the task by clicking the "Delete" button and close the "Update Task" window with the "Close" button discarding any changes.

Changing the "status" of the task will move the task to the corresponding list.

Below each list there are a dropdown menu and two buttons.

The dropdown menu allows user to choose the way the tasks are sorted.
"a" stands for "ascending" and "d" for "descending".

The "Filter" button functionality is not yet implemented.

The "Delete All" button allows user to delete all tasks from the list. The user is presented with confirmation dialog where they can either proceed with the deletion or cancel it.